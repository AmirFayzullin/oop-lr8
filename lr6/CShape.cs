﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace lr7
{
    abstract class CShape : IShape, ISerializable<IShape>
    {
        protected int x, y;
        protected int width;
        protected int height;
        protected float scaling = 1;
        protected Colors color;
        protected bool selected = false;
        protected CanvasDimensions dimensions;
        protected bool sticky = false;
        protected string name;
        EventHandler observers;

        public int X { get { return x; } }
        public int Y { get { return y; } }
        public int Width { get { return width; } }
        public int Height { get { return height; } }
        public bool Sticky { get { return sticky; } }
        public string Name { get { return name; } }

        protected CShape() {
        }
        protected CShape(int _x, int _y, Colors c, CanvasDimensions d)
        {
            x = _x;
            y = _y;
            color = c;
            dimensions = d;
        }

        virtual public bool isSelected() { return selected; }

        virtual public void select(bool toSelect) { selected = toSelect; }

        virtual public bool setColor(Colors newColor) { color = newColor; return true; }
        virtual public Colors getColor() { return color; }
        virtual public float getScale() { return scaling; }

        virtual public bool setScale(float k) { scaling = k; return true; }

        virtual public void shift(int dx, int dy) {
            if (isOutOfBox(x + dx, y + dy)) return;
            x += dx; y += dy;
            notifyObservers();
        }

        abstract public bool hittest(int x, int y);

        abstract public bool isOutOfBox(int x, int y);

        abstract public void render(Graphics g);

        virtual public List<IShape> getShapes()
        {
            return new List<IShape>();
        }

        virtual public List<IShape> ungroup()
        {
            return new List<IShape>();
        }

        protected Color mapToColor()
        {
            Color c;
            switch (color)
            {
                case Colors.Red:
                    c = Color.Red;
                    break;
                case Colors.Green:
                    c = Color.Green;
                    break;
                case Colors.Blue:
                    c = Color.Blue;
                    break;
                default:
                    c = Color.Red;
                    break;
            }

            return c;
        }

        public virtual void addShape(IShape s)
        {

        }

        abstract public ShapeBounds getBounds();

        abstract public void load(StreamReader s, IFactory<IShape> f);

        abstract public void save(StreamWriter s);

        public void subscribe(EventHandler handler)
        {
            observers += handler;
        }
                
        public void unsubscribe(EventHandler handler)
        {
            observers -= handler;
        }

        private void notifyObservers()
        {
            if (observers == null) return;
            observers.Invoke(this, null);
        }

        public bool isTouched(IShape s)
        {
            int x0 = X - Width / 2;
            int x1 = X + Width / 2;
            int y0 = Y - Height / 2;
            int y1 = Y + Height / 2;

            int sx0 = s.X - s.Width / 2;
            int sx1 = s.X + s.Width / 2;
            int sy0 = s.Y - s.Height / 2;
            int sy1 = s.Y + s.Height / 2;
            
            return (
                    (
                      (
                        (x0 >= sx0 && x0 <= sx1) || (x1 >= sx0 && x1 <= sx1)
                      ) && (
                        (y0 >= sy0 && y0 <= sy1) || (y1 >= sy0 && y1 <= sy1)
                      )
                    ) || (
                      (
                        (sx0 >= x0 && sx0 <= x1) || (sx1 >= x0 && sx1 <= x1)
                      ) && (
                        (sy0 >= y0 && sy0 <= y1) || (sy1 >= y0 && sy1 <= y1)
                      )
                    )
                  ) || (
                    (
                      (
                        (x0 >= sx0 && x0 <= sx1) || (x1 >= sx0 && x1 <= sx1)
                      ) && (
                        (sy0 >= y0 && sy0 <= y1) || (sy1 >= y0 && sy1 <= y1)
                      )
                    ) || (
                      (
                        (sx0 >= x0 && sx0 <= x1) || (sx1 >= x0 && sx1 <= x1)
                      ) && (
                        (y0 >= sy0 && y0 <= sy1) || (y1 >= sy0 && y1 <= sy1)
                      )
                    )
                );
        }
    }

    class CShapesFactory : IFactory<IShape>
    {
        public IShape create(string code)
        {
            string[] parameters = code.Split(" ".ToCharArray());
            string shapeCode = parameters[0];
            bool isSticky = false;
            if (parameters[0] == "Sticky")
            {
                isSticky = true;
                shapeCode = parameters[1];
            }

            IShape s = null;
            switch (shapeCode)
            {
                case "C":
                    s = new CCircle();
                    break;
                case "S":
                    s = new CSquare();
                    break;
                case "T":
                    s = new CTriangle();
                    break;
                case "<":
                    s = new CShapesGroup(new List<IShape>());
                    break;
                default:
                    s = null;
                    break;
            }

            return isSticky ? new CStickyShape(s) : s;
        }
    }
}
