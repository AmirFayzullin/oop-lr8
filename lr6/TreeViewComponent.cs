﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lr7
{
    class TreeViewComponent
    {
        TreeView component;
        IShapesManager manager;
        int id = 0;
        public TreeViewComponent(TreeView c, IShapesManager m)
        {
            component = c;
            component.AfterSelect += handleSelect;
            manager = m;
            manager.subscribe(updateHandler);
            render();
        }

        void render()
        {
            IStore<IShape> shapes = manager.getShapes();
            clear();
            id = 0;
            if (shapes.Count == 0) return;
            for (shapes.First(); !shapes.IsEol(); shapes.Next())
                processNode(shapes.GetCurrent());
        }

        void processNode(IShape shape, TreeNode n = null)
        {
            TreeNode newNode = new TreeNode(shape.Name);
            id++;
            if (n != null) n.Nodes.Add(newNode);
            else component.Nodes.Add(newNode);

            if (shape.isSelected())
            {
                component.AfterSelect -= handleSelect;
                component.SelectedNode = newNode;
                component.AfterSelect += handleSelect;
            }

            List<IShape> nested = shape.getShapes();

            for (int i = 0; i < nested.Count; i++)
                processNode(nested[i], newNode);
           
        }

        void clear()
        {
            component.Nodes.Clear();
        }

        void updateHandler(object sender, EventArgs e)
        {
            render();
        }

        void handleSelect(object sender, EventArgs e)
        {
            TreeNode selected = component.SelectedNode;
            while (selected.Parent != null) selected = selected.Parent;
            int i = 0;
            IStore<IShape> store = manager.getShapes();
            for (store.First(); i < component.Nodes.Count && component.Nodes[i] != selected; i++, store.Next());
            manager.selectShape(store.GetCurrent());

        }

        ~TreeViewComponent()
        {
            manager.unsubscribe(updateHandler);
        }
    }
}
