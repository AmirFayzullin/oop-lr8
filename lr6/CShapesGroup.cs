﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace lr7
{
    class CShapesGroup : CShape
    {
        delegate void applyCb(IShape s);
        List<IShape> shapes = new List<IShape>();

        public CShapesGroup(List<IShape> _shapes) 
        {
            name += "Group ";
            for (int i = 0; i < _shapes.Count; i++) addShape(_shapes[i]);
            apply((IShape s) => selected |= s.isSelected());
            apply((IShape s) => s.select(false));
            getBounds();
            color = getColor();
        }

        public override void addShape(IShape shape)
        {
            if ((shape as CStickyShape) != null) shapes.Add((shape as CStickyShape).getShape());
            else shapes.Add(shape);
            getBounds();
        }

        public override bool isSelected()
        {
            return selected;
        }

        public override List<IShape> getShapes()
        {
            return shapes;
        }

        public override void select(bool toSelect)
        {
            selected = toSelect;
        }

        public override Colors getColor()
        {
            Colors c = Colors.Blue;
            apply((IShape s) => c = s.getColor());
            return c;
        }

        public override float getScale()
        {
            float scaling = 1;
            apply((IShape s) => scaling = s.getScale());
            return scaling;
        }

        public override bool setColor(Colors c)
        {
            apply((IShape s) => s.setColor(c));
            return true;
        }

        public override bool setScale(float scaling)
        {
            bool success = true;
            for (int i = 0; i < shapes.Count && success; i++)
            {
                float prev = shapes[i].getScale();
                shapes[i].setScale(scaling);
                if (shapes[i].getScale() == prev) success = false; // out of box
                shapes[i].setScale(prev);
            }

            if (!success) return false;

            for (int i = 0; i < shapes.Count; i++) shapes[i].setScale(scaling);
            return true;            
        }

        public override void shift(int dx, int dy)
        {
            bool isOut = isOutOfBox(X + dx, Y + dy);
            if (!isOut) apply((IShape s) => s.shift(dx, dy));
            base.shift(dx, dy);
        }

        public override bool isOutOfBox(int x, int y)
        {
            bool isOut = false;
            apply((IShape s) => isOut |= s.isOutOfBox(s.X + x - X, s.Y + y - Y));
            return isOut;
        }

        public override bool hittest(int x, int y)
        {
            bool hit = false;
            apply((IShape s) => hit |= s.hittest(x, y));
            return hit;
        }

        public override void render(Graphics g)
        {
            apply((IShape s) => s.render(g));

            if (!selected) return;

            ShapeBounds bounds = getBounds();
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            Pen p = new Pen(Color.Black);
            g.DrawRectangle(p, new Rectangle(bounds.x0 - 10, bounds.y0 - 10, bounds.x1 - bounds.x0 + 20, bounds.y1 - bounds.y0 + 20));
            p.Dispose();
        }

        private void apply(applyCb cb)
        {
            for (int i = 0; i < shapes.Count; i++) cb(shapes[i]);
        }

        public override void load(StreamReader stream, IFactory<IShape> factory)
        {
            string code = stream.ReadLine();
            while (code != ">")
            {
                IShape s = factory.create(code);
                if (s == null) continue;
                s.load(stream, factory);
                addShape(s);
                code = stream.ReadLine();
            }
        }

        public override void save(StreamWriter stream)
        {
            stream.WriteLine("<");
            apply((IShape s) => s.save(stream));
            stream.WriteLine(">");
        }

        public override List<IShape> ungroup()
        {
            apply((IShape s) => s.select(selected));
            return shapes;
        }

        public override ShapeBounds getBounds()
        {
            ShapeBounds bounds = new ShapeBounds(9999, -1, 9999, -1);
            
            for(int i = 0; i < shapes.Count; i++)
            {
                ShapeBounds nestedShapeBounds = shapes[i].getBounds();
                if (nestedShapeBounds.x0 < bounds.x0) bounds.x0 = nestedShapeBounds.x0;
                if (nestedShapeBounds.x1 > bounds.x1) bounds.x1 = nestedShapeBounds.x1;
                if (nestedShapeBounds.y0 < bounds.y0) bounds.y0 = nestedShapeBounds.y0;
                if (nestedShapeBounds.y1 > bounds.y1) bounds.y1 = nestedShapeBounds.y1;
            }

            x = bounds.x0 + (bounds.x1 - bounds.x0) / 2;
            y = bounds.y0 + (bounds.y1 - bounds.y0) / 2;
            width = bounds.x1 - bounds.x0;
            height = bounds.y1 - bounds.y0;

            return bounds;
        }
    }
}
