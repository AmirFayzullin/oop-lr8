﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lr7
{
	delegate bool cmpFn<T>(T item);
	interface IStore<T>
	{
		void Add(T value);
		int Count { get; }
		T GetCurrent();
		T RemoveCurrent();
		void First();
		void Next();
		void Last();
		void Prev();
		bool IsEol();
		bool Contains(cmpFn<T> cmpcb);
		void Clear();
		void Save(string filename);
		void Load(string filename, IFactory<T> factory);
	}
}
