﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace lr7
{
    class CStickyShape : IShape
    {
        IShape shape;
        IShapesManager manager;
        List<IShape> stickied = new List<IShape>();

        public int X { get { return shape.X; } }
        public int Y { get { return shape.Y; } }
        public int Width { get { return shape.Width; } }
        public int Height { get { return shape.Height; } }
        public bool Sticky { get { return true; } }
        public string Name { get { return "Sticky " + shape.Name; } }
        public CStickyShape(IShape s)
        {
            shape = s;
        }

        public void init( IShapesManager _manager)
        {
            manager = _manager;
            manager.subscribe(managerUpdateHandler);
            subscribeShapes();
        }

        public List<IShape> getShapes()
        {
            return shape.getShapes();
        }

        public void render(Graphics g)
        {
            shape.render(g);
        }

        public IShape getShape() { return shape; }

        public Colors getColor() { return shape.getColor(); }
        public float getScale() { return shape.getScale(); }
        public bool setColor(Colors color) { return shape.setColor(color); }
        public bool setScale(float scaling) { return shape.setScale(scaling); }

        public void addShape(IShape s) { }
        public List<IShape> ungroup() { return shape.ungroup(); }
        public bool isTouched(IShape s) { return shape.isTouched(s); }

        public void shift(int dx, int dy)
        {
            if (shape.isOutOfBox(shape.X + dx, shape.Y + dy)) return;

            IStore<IShape> shapes = manager.getShapes();
            for (shapes.First(); !shapes.IsEol(); shapes.Next()) checkCandidate(shapes.GetCurrent());

            shape.shift(dx, dy);
            for (int i = 0; i < stickied.Count; i++) stickied[i].shift(dx, dy);
        }
        public void subscribe(EventHandler handler)
        {

        }

        public void unsubscribe(EventHandler handler)
        {

        }

        public bool isOutOfBox(int x, int y)
        {
            return shape.isOutOfBox(x, y);
        }

        public bool hittest(int x, int y)
        {
            return shape.hittest(x, y);
        }

        public void select(bool toSelect)
        {
            shape.select(toSelect);
        }

        public bool isSelected()
        {
            return shape.isSelected();
        }

        public void load(StreamReader s, IFactory<IShape> f)
        {
            shape.load(s, f);
        }

        public ShapeBounds getBounds()
        {
            return shape.getBounds();
        }

        public void save(StreamWriter s)
        {
            s.Write("Sticky ");
            shape.save(s);
        }

        void subscribeShapes()
        {
            IStore<IShape> shapes = manager.getShapes();
            for (shapes.First(); !shapes.IsEol(); shapes.Next()) shapes.GetCurrent().subscribe(shapeUpdateHandler);
        }

        void unsubscribeShapes()
        {
            IStore<IShape> shapes = manager.getShapes();
            for (shapes.First(); !shapes.IsEol(); shapes.Next()) shapes.GetCurrent().unsubscribe(shapeUpdateHandler);
        }

        void addToStickied(IShape s)
        {
            if (stickied.Contains(s)) return;
            stickied.Add(s);
        }

        void removeFromStickied(IShape s)
        {
            if (!stickied.Contains(s)) return;
            stickied.Remove(s);
        }

        void shapeUpdateHandler(object sender, EventArgs e)
        {
            checkCandidate(sender as IShape);
        }

        void checkCandidate(IShape candidate)
        {
            if (candidate == null || candidate == this || candidate as CStickyShape != null) return;

            if (candidate.isTouched(shape)) addToStickied(candidate);
            else removeFromStickied(candidate);
        }

        void managerUpdateHandler(object sender, EventArgs e)
        {
            unsubscribeShapes();
            subscribeShapes();
        }

        ~CStickyShape()
        {
            unsubscribeShapes();
        }
    }
}
