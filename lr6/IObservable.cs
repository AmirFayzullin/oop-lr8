﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lr7
{
    interface IObservable
    {
        void subscribe(EventHandler handler);
        void unsubscribe(EventHandler handler);
    }
}
